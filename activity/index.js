// 1. Code Debugging

let fullname = "Steve Rogers";
	console.log(fullname);

	let userAge = 40;
	console.log(userAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}

	console.log(profile);

	let fullname2 = "Tony Stark";
	console.log(fullname2);

	/*const largestOcean = "Pacific Ocean";*/
	let largestOcean = "Atlantic Ocean";
	console.log(largestOcean);



// 2. Create function; Sum of two Numbers

	function getSum (num1, num2){
		let sum1 = num1 + num2;
		console.log(`The sum of ${num1} and ${num2} is ${sum1}`)
	}

	getSum(5,9);



