/*
Javascript is a scripting programming language that enables us to make interactive web pages
Two ways to add comments
 // ctrl + /  single line comment
ctrl + shift + / multi line comment

*/

console.log("Hello, World!");
/* console log allows us to show data in our console
	

*/
console.log("Charles Bon Chua")
console.log("Seafoods");

/*
statements  are instructions expressions we add to our program which will then be communicated to our computers and interpret the instructions and perform the task.

syntax are set of rules that describes how statements or instructions are properly made/constructed

Variables are containers of data
*/
let name = "Charles Bon Chua";
console.log(name);

let num = 5;
let num2 = 10;

console.log(num);
console.log(num2);

/*
The value of variables will be shown.
We can also display multiple variables
We can actually create variables without initial value but assigned as undefined 
Creating a variable
Declaration - creation of variable with either let or const keyword
Initialization - when we provide an initial value to our variable

Declaration = Initialization
let myVar   = "intial value"



*/
console.log(name,num,num2);
//console.log(name2);

let myVariable;
console.log(myVariable);

myVariable = "new value";
console.log(myVariable);

let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);
//variables can be updated, not using let keyword

bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

/*let bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);*/

// we cannot update with the let keyword
// create another variable with the same name

/*
Const 
Const keyword  allow us to create variable but cannot be updated


*/


const pi = 3.1416;
console.log(pi);

/*pi = 3.15;
console.log(pi);*/

/*
const plateNum;
console.log(plateNum);
Const without initialization results to an error

*/

let name2 = "Edward Cullen";
let role = "Supervisor";

role = "Director";
const tin = "12333-1234";
console.log(name2,role,tin);

/*
Conventions in creating variable 
usually named in small caps and camelCase
Name your variables appropriately

In programming languages, data is differentiated into diffeent types

to create strings we use '' ""
to create objects we use {}
To create arrays we use []


*/

/*
Strings are a series of alphanumeric  character that create a word, a phrase, a name or anything that is related to creating a text.
Strings should not hold or used mathematical operations 
*/

let country = "Philippines";
let province = "Metro Manila";
console.log(province, country);

/*
We can combine strings using concatenation
*/
let address = province + ", " + country;
console.log(address);


let city1 = "Manila";
let city2 = "Copenhagen";
let city3 = "Washington D.C.";
let city4 = "Tokyo";
let city5 = "New York";
let country1 = "Philippines";
let country2 = "U.S.A.";
let country3 = "South Korea";
let country4 = "Japan";

let capital1 = city1 + ", " + country1;
let capital2 = city3 + ", " + country2;
let capital3 = city4 + ", " + country4;

console.log(capital1);
console.log(capital2);
console.log(capital3);

let number1 = 10;
let number2 = 6;

console.log(number1);
console.log(number2);

/*
(+) used to add numbers 
*/

let sum1 = number1 + number2;
console.log(sum1);
let sum2 = 16 + 4;
console.log(sum2);
let numString1 ="30";
let numString2 = "50";

let sumString1 = numString1 + numString2;
console.log(sumString1);

let sum3 = number1 + numString2;
console.log(sum3);

/*
Boolean is used for logical operations and if else condition
Usually yes or no question

*/

let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("Is she married? " + isMarried);
console.log("Is Curry an MVP? " + isMVP);
console.log("Is hee the current admin? " + isAdmin);

/*
Arrays are special data types used to store multiple values
Good arrays have same data types and same theme
Array values are separated by a coma
*/

let array1 = ["Goku", "Gohan", "Goten", "Vegeta"];
let array2 = ["One punch Man", "Saitama", true, 5000];

console.log(array1);
console.log(array2);

/*
Objects are special data types used to mimic real world items
Used to create complex data structure that contain pieces of information related to each other 

Used to group different data types
*/

let hero1 = {
	heroName: "One Punch Man",
	realName: "Saitama",
	income: "5000",
	isActive: true,
}

let  itzy = [ "Ryujin", "Yeji", "Lia", "Yuna", "Chaeryoung"];
let person = {
	firstName: "Charles",
	lastName: "Chua",
	isDeveloper: true,
	age: 400,
}

console.log(itzy);
console.log(person);

/*
Null and undefined
Null - the variable contains nothing
Undefined - variable has no initial value yet

Uses of Null
-when doing a query, zero results,
let foundresult = null
*/

let person2 = {
	name: "Peter",
	age: 42,
}
//To accesss the value of an object, we use dot notation

console.log(person2.name);
console.log(person2.age);
console.log(person2.isAdmin);

/*
Functions
are lines of codes to tell our device to perform a certain task
These are reusable pieces of codes which can be used over and over again


*/

console.log("Good afternoon, Everyone! Welcome to my application!");

function greet() {
	console.log("Good afternoon, Everyone! Welcome to my application!");	
}

greet();
greet();
/*
function invocation is when we  call or use our functions
*/

let favefood = "Seafoods";
let numA = 150;
let numB = 9;
let sumA = numA + numB;
let numC = 100;
let numD = 90;
let prodA = numC * numD;
let isUserActive = true;

let resto = ["Jollibee", "McDo", "KFC", "Jco", "Infinitea"];
let MamiKawada = {
	firstN: "Mami",
	lastN: "Kawada",
	stageN: "Mami Kawada",
	birthDay: "February 13, 1980",
	age: 42,
	bestAlbum: "Birth",
	bestSong: "Joint",
	bestTVShow: "Shakugan no Shana",
	bestMovie: null,
	isUserActive: true,
}

console.log(favefood);
console.log(sumA);
console.log(prodA);
console.log(isUserActive);

console.log(resto);
console.log(MamiKawada);

/*
Parameters and Argumaents

function printName (firstN){
	console.log(`My name is ${name}`)
};

firstN refers to parameter. Act a named variable or container that exist only in the function
Name is parameter (representation of argument), when it is invoked, it is an argument (data passed in as function)



*/

function printName (name){
	console.log(`My name is ${name}`)
};
console.log(name);
printName("Jake");

function displayNum (number){
	console.log(number)
};

displayNum (3000);
displayNum (3001);

function messageA (language){
	console.log(`${language} is fun`)
};

messageA("Javascript");

let language = "C";
console.log(language);

//Multiple Parameters and Arguments

function displayFullName (firstNm, lastNm, age){
	console.log(`${firstNm} ${lastNm} is ${age}`)
};

displayFullName("Charles", "Chua", 25)
/*
a function can also receive either a single or multiple arguments as long as it the same number of parameters
*/

/*
Return keyword
use to return a value,
stops the process of the function after the return keyword

*/

function createFullName (firstName, middleName, lastName){
	return `${firstName} ${middleName} ${lastName} `
	console.log("I will no longer run, because the function value has been returned")
}

let fullName1 = createFullName("Tom", "Cruise", "Mapother");
console.log(fullName1);

createFullName("Tom", "Cruise", "Mapother");